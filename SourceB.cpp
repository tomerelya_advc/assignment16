#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

unordered_map<string, vector<string>> results;
void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

void printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

int getLastId(void* notUsed, int argc, char** argv, char** azCol)
{
	cout << "the Last id is: " << argv[0] << endl;
	return 0;
}

void sendCMD(string cmd, sqlite3* db, char* zErrMsg)
{
	int rc;

	rc = sqlite3_exec(db, cmd.c_str(), callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		exit(0);
	}
}

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	int rc;
	string temp;
	bool flag = true;
	int price, bal, avail;
	temp = "select balance from accounts where buyer_id = \"";
	temp += std::to_string(buyerid);
	temp += "\"";
	
		

	rc = sqlite3_exec(db, temp.c_str() , callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	temp = results.find("balance")->second.at(0);
	bal = atoi(temp.c_str());
	cout << endl << "Balance: " << atoi(temp.c_str()) << endl;

	system("Pause");
	system("CLS");
	
	//////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////

	temp = "select price from cars where id = \"";
	temp += std::to_string(carid);
	temp += "\"";

	rc = sqlite3_exec(db, temp.c_str(), callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	temp = results.find("price")->second.at(0);
	price = atoi(temp.c_str());
	cout << endl << "Price: " << atoi(temp.c_str()) << endl;


	system("Pause");
	system("CLS");

	///////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////

	if (bal < price)
		return false;

	temp = "select available from cars where id = \"";
	temp += std::to_string(carid);
	temp += "\"";

	rc = sqlite3_exec(db, temp.c_str(), callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	temp = results.find("available")->second.at(0);
	avail = atoi(temp.c_str());
	cout << endl << "Availability: " << atoi(temp.c_str()) << endl;
	if (avail == 0)
		return false;
	else
	{
		temp = "update cars set available =\"0\" where id = \"";
		temp += std::to_string(carid);
		temp += "\"";

		rc = sqlite3_exec(db, temp.c_str(), callback, 0, &zErrMsg);

		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
	}

	return true;

	system("Pause");
	system("CLS");
}

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	string temp;
	int firstBal, secBal;
	sendCMD("begin transaction", db, zErrMsg);
	temp = "select balance from accounts where id = \"";
	temp += std::to_string(from);
	temp += "\"";
	sendCMD(temp, db, zErrMsg);
	firstBal = atoi(results.find("balance")->second.at(0).c_str());
	
	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	
	temp = "select balance from accounts where id = \"";
	temp += std::to_string(to);
	temp += "\"";
	sendCMD(temp, db, zErrMsg);
	secBal = atoi(results.find("balance")->second.at(1).c_str());

	/////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////

	if (firstBal - amount < 0)
		return false;
	cout << "First Balance: " << firstBal << endl << "second bal: " << secBal << endl;
	firstBal = firstBal - amount;
	secBal = secBal + amount;
	cout << "First Balance: " << firstBal << endl << "second bal: " << secBal << endl;

	//////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////

	temp = "update accounts set balance = \"";
	temp += std::to_string(firstBal);
	temp += "\" where id = \"";
	temp += std::to_string(from);
	temp += "\"";
	sendCMD(temp, db, zErrMsg);

	///////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////

	temp = "update accounts set balance = \"";
	temp += std::to_string(secBal);
	temp += "\" where id = \"";
	temp += std::to_string(to);
	temp += "\"";
	sendCMD(temp, db, zErrMsg);
	sendCMD("commit",db,zErrMsg);

	return true;

}

int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;
	int buyID = 0;
	int vehID = 0;
	int amount = 0;

	// connection to the databa-se
	rc = sqlite3_open("carsDealer.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}
	cout << "Please enter the buyer's ID: ";
	cin >> buyID;
	cout << "Please enter the vehicle's ID: ";
	cin >> vehID;

	flag = carPurchase(buyID, vehID, db, zErrMsg);
	cout << "Results: " << flag << endl;

	cout << "The following are examples.";
	system("PAUSE");
	system("CLS");

	flag = carPurchase(1, 23, db, zErrMsg);
	cout << "Results: " << flag << endl;
	system("PAUSE");
	system("CLS");
	flag = carPurchase(12, 21, db, zErrMsg);
	cout << "Results: " << flag << endl;
	system("PAUSE");
	system("CLS");
	flag = carPurchase(3, 20, db, zErrMsg);
	cout << "Results: " << flag << endl;

	cout << "Enter the ID of the sender: ";
	cin >> buyID;
	cout << "Enter the ID of the receiver: ";
	cin >> vehID;
	cout << "Enter the amount to transfer: ";
	cin >> amount;

	flag = balanceTransfer(buyID, vehID, amount, db, zErrMsg);
	cout << "Results:" << flag << endl;



	system("PAUSE");
	return(0);


}